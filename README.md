
	-  have a number of connected vehicles that belongs to a number of customers.
	-  have a need to be able to view the status of the connection among these vehicles on a monitoring display.
	-  vehicles send the status of the connection one time per minute.
	-  status can be compared with a ping (network trace); no request from the vehicle means no connection. 
	-  vehicle is either Connected or Disconnected.