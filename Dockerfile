FROM mongo:4.0.3

ENV MONGO_INITDB_ROOT_USERNAME admin
ENV MONGO_INITDB_ROOT_PASSWORD admin
ENV MONGO_INITDB_DATABASE admin

COPY mongo-init.js /docker-entrypoint-initdb.d/

FROM java:8-jdk-alpine
COPY /build/libs/challenge-0.0.1-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
RUN sh -c 'touch challenge-0.0.1-SNAPSHOT.jar'
ENTRYPOINT ["java" ,"-jar","challenge-0.0.1-SNAPSHOT.jar"]








#FROM dockerfile/ubuntu
#VOLUME /tmp
#ARG DEPENDENCY=target/dependency
#COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
#COPY ${DEPENDENCY}/META-INF /app/META-INF
#COPY ${DEPENDENCY}/BOOT-INF/classes /app
#ENTRYPOINT ["java","-cp","app:app/lib/*","se.alten.challenge.Application"]