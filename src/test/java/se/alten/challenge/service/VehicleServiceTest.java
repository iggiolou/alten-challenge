package se.alten.challenge.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.service.api.VehicleService;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleServiceTest {

    @Autowired
    VehicleService vehicleService;

    @Test
    public void findAll(){
       List<VehicleDto> vehicleDtos =  vehicleService.findAll();
       vehicleDtos.forEach(System.out::println);
    }




}
