package se.alten.challenge.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.alten.challenge.dto.CustomerDto;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.service.api.CustomerService;


import java.util.ArrayList;
import java.util.List;

import static se.alten.challenge.util.ObjectUtil.setUpCustomerDto;
import static se.alten.challenge.util.ObjectUtil.setUpVehicleDto;


/**
 * @author viktoriyadoroshenko
 * @since 2019-05-27
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceTest {

    @Autowired
    CustomerService customerService;

    @Test
    public void saveCustomerTest(){
        CustomerDto customerDto = setUpCustomerDto();
        VehicleDto vehicleDto = setUpVehicleDto();
        vehicleDto.setCustomer(customerDto);
        List<VehicleDto> vehicleDtoList = new ArrayList<>();
        vehicleDtoList.add(vehicleDto);
        customerDto.setVehicles(vehicleDtoList);

        CustomerDto saved = customerService.save(customerDto);
        Assert.assertEquals(customerDto.getName(), saved.getName());
        Assert.assertEquals(customerDto.getAddress(), saved.getAddress());
        Assert.assertEquals(customerDto.getVehicles(), saved.getVehicles());
        Assert.assertNotNull(saved.getId());

    }


}
