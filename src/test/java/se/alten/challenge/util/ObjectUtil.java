package se.alten.challenge.util;

import se.alten.challenge.dto.CustomerDto;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.entity.Customer;
import se.alten.challenge.entity.Vehicle;

import java.util.UUID;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-27
 */
public class ObjectUtil {



    public static Customer setUpCustomer(){
        String id = UUID.randomUUID().toString();
        Customer entity = new Customer();
        entity.setId(id);
        entity.setAddress("Test Address");
        entity.setName("Test First Name");
        return entity;

    }


    public static CustomerDto setUpCustomerDto(){
        CustomerDto customerDto = new CustomerDto();
        customerDto.setName("Matias Belle");
        customerDto.setAddress("Balkvägen 34, 112 15 Stockholm");
        return customerDto;
    }

    public static VehicleDto setUpVehicleDto(){
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setRegNo("AAL3846");
        vehicleDto.setVin("YS2R4X20115329403");
        return vehicleDto;
    }

    public static Vehicle setUpVehicle(){
        Vehicle vehicle = new Vehicle();
        String id = UUID.randomUUID().toString();
        vehicle.setId(id);
        vehicle.setRegNo("DEF456");
        vehicle.setVin("VKUR4X20249094388");
        return vehicle;
    }

}
