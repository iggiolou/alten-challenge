package se.alten.challenge.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.alten.challenge.entity.Vehicle;

import static se.alten.challenge.util.ObjectUtil.setUpVehicle;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-02
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleRepositoryTest {

    @Autowired
    VehicleRepository vehicleRepository;

    @Test
    public void createVehicle(){
        Vehicle vehicle = setUpVehicle();
        vehicleRepository.save(vehicle);

    }

    @Test
    public void findAllVehicles(){
        vehicleRepository.findAll();
    }
}
