package se.alten.challenge.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.alten.challenge.entity.Customer;
import se.alten.challenge.entity.Vehicle;

import java.util.HashSet;
import java.util.Set;

import static se.alten.challenge.util.ObjectUtil.setUpCustomer;
import static se.alten.challenge.util.ObjectUtil.setUpVehicle;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VehicleRepository vehicleRepository;

    @Test
    public void saveCustomerTest(){
        Customer customer = setUpCustomer();
        customerRepository.save(customer);
    }


    @Test
    public void test(){
        Customer customer = setUpCustomer();
        Vehicle vehicle = setUpVehicle();
        vehicle.setCustomerId(customer.getId());
        Set<Vehicle> vehicleDtoList = new HashSet<>();
        vehicleDtoList.add(vehicle);
        customer.setVehicles(vehicleDtoList);
        Customer saved = customerRepository.save(customer);
        Vehicle saved1 = vehicleRepository.save(vehicle);
        System.out.println("bla");

    }




}
