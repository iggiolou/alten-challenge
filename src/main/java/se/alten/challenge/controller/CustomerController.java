package se.alten.challenge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import se.alten.challenge.repository.CustomerRepository;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */
@RestController("/customer")
public class CustomerController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/all")
    public ResponseEntity<List> findAll() {
        return new ResponseEntity<>(customerRepository.findAll(), HttpStatus.OK);
    }
}
