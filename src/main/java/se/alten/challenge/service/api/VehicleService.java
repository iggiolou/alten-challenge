package se.alten.challenge.service.api;

import se.alten.challenge.dto.VehicleDto;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-27
 */

public interface VehicleService {

    List<VehicleDto> findAll();

    VehicleDto findById(String id);

    VehicleDto save(VehicleDto vehicleDto);

    void delete(VehicleDto vehicleDto);

    void deleteById(String id);

    VehicleDto update(VehicleDto vehicleDto);

    VehicleDto findByVin(String vin);

}
