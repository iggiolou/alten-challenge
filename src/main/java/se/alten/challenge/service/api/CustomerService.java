package se.alten.challenge.service.api;

import se.alten.challenge.dto.CustomerDto;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-27
 */

public interface CustomerService {

    List<CustomerDto> findAll();

    CustomerDto findById(String id);

    CustomerDto save(CustomerDto customerDto);

    void delete(CustomerDto customerDto);

    CustomerDto update(CustomerDto customerDto);

    void deleteById(String id);

}
