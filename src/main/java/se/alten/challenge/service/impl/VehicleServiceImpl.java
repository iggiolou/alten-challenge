package se.alten.challenge.service.impl;

import com.vaadin.flow.router.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.entity.Vehicle;
import se.alten.challenge.mapping.VehicleMapper;
import se.alten.challenge.repository.VehicleRepository;
import se.alten.challenge.service.api.CustomerService;
import se.alten.challenge.service.api.VehicleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */
@Service
public class VehicleServiceImpl implements VehicleService {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    CustomerService customerService;

    @Autowired
    VehicleMapper vehicleMapper;

    public List<VehicleDto> findAll() {
        List<Vehicle> vehicles = vehicleRepository.findAll();
        List<VehicleDto> vehicleDtos = new ArrayList<>();
        vehicles.forEach(x -> vehicleDtos.add(vehicleMapper.toDto(x)));
        return vehicleDtos;
    }

    public VehicleDto findById(String id) {
        return vehicleMapper.toDto(vehicleRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Vehicle with id " + id + " not found.")));
    }

    @Override
    public VehicleDto save(VehicleDto vehicleDto) {
        Vehicle vehicle = vehicleRepository.save(vehicleMapper.toEntity(vehicleDto));
        return vehicleMapper.toDto(vehicle);

    }

    @Override
    public void delete(VehicleDto vehicleDto) {
        vehicleRepository.delete(vehicleMapper.toEntity(vehicleDto));

    }

    @Override
    public void deleteById(String id) {

        if (vehicleRepository.findById(id).isPresent()) {
            vehicleRepository.deleteById(id);
        }

    }

    @Override
    public VehicleDto update(VehicleDto vehicleDto) {

        Optional<Vehicle> vehicle = vehicleRepository.findByVin(vehicleDto.getVin());

        if (vehicle.isPresent()) {
            VehicleDto found = vehicleMapper.toDto(vehicle.get());
            found.setCustomer(vehicleDto.getCustomer());
            found.setVin(vehicleDto.getVin());
            found.setRegNo(vehicleDto.getRegNo());
            vehicleRepository.save(vehicleMapper.toEntity(found));
            return found;
        } else {
            return save(vehicleDto);
        }


    }

    @Override
    public VehicleDto findByVin(String vin) {
        return vehicleMapper.toDto(vehicleRepository.findByVin(vin).orElseThrow(() -> new NotFoundException("Vehicle with vin " + vin + " not found.")));
    }

}
