package se.alten.challenge.service.impl;

import com.vaadin.flow.router.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.alten.challenge.dto.CustomerDto;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.entity.Customer;
import se.alten.challenge.mapping.CustomerMapper;
import se.alten.challenge.repository.CustomerRepository;
import se.alten.challenge.service.api.CustomerService;
import se.alten.challenge.service.api.VehicleService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private CustomerMapper customerMapper;

    public List<CustomerDto> findAll() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerDto> customerDtos = new ArrayList<>();
        customers.forEach(x -> customerDtos.add(customerMapper.toDto(x)));
        return customerDtos;
    }

    public CustomerDto findById(String id) {
        if (id != null && !id.equals("")) {
            return customerMapper.toDto(customerRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException("No customer with id " + id)));
        } else {
            return null;
        }
    }

    @Override
    public CustomerDto save(CustomerDto customerDto) {
        List<VehicleDto> vehicleDtos = new ArrayList<>();
        if (customerDto.getVehicles() != null) {
            customerDto.getVehicles().forEach(vehicle -> {
                VehicleDto saved = vehicleService.save(vehicle);
                vehicleDtos.add(saved);

            });
        }
        customerDto.setVehicles(vehicleDtos);
        Customer customer = customerRepository.save(customerMapper.toEntity(customerDto));
        return customerMapper.toDto(customer);
    }

    @Override
    public void delete(CustomerDto customerDto) {
        customerRepository.delete(customerMapper.toEntity(customerDto));
    }

    @Override
    public CustomerDto update(CustomerDto customerDto) {
        Optional<Customer> customer = customerRepository.findById(customerDto.getId());
        if (customer.isPresent()) {
            CustomerDto found = customerMapper.toDto(customer.get());
            found.setAddress(customerDto.getAddress());
            found.setName(customerDto.getName());
            found.setId(customerDto.getId());
            found.setVehicles(customerDto.getVehicles());
            save(found);
            return found;
        } else {
            return save(customerDto);
        }


    }

    @Override
    public void deleteById(String id) {

        if (customerRepository.findById(id).isPresent()) {
            customerRepository.deleteById(id);
        }


    }


}
