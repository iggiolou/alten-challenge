package se.alten.challenge.entity;

import com.mongodb.lang.Nullable;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Document
@Data
public class Vehicle {

    @Id
    private String id;
    @Nullable
    private String customerId;
    private String vin;
    private String regNo;


}
