package se.alten.challenge.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Document
@Data
public class Customer {

    @Id
    private String id;
    private String name;
    private String address;
    private Set<Vehicle> vehicles;

}
