package se.alten.challenge.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.entity.Vehicle;
import se.alten.challenge.service.api.CustomerService;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class VehicleMapper {


    @Autowired
    public CustomerService customerService;


    @Mapping(expression = "java(customerService.findById(vehicle.getCustomerId()))", target = "customer")
    public abstract VehicleDto toDto(Vehicle vehicle);

    @Mapping(expression = "java(vehicleDto.getCustomer() == null ? \"\" : vehicleDto.getCustomer().getId())", target = "customerId")
    public abstract Vehicle toEntity(VehicleDto vehicleDto);

}
