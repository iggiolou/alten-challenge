package se.alten.challenge.mapping;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import se.alten.challenge.dto.CustomerDto;
import se.alten.challenge.entity.Customer;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CustomerMapper {

    CustomerDto toDto(Customer customer);

    Customer toEntity(CustomerDto customerDto);
}
