package se.alten.challenge.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.lang.Nullable;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Data
public class VehicleDto {

    private String id;

    @JsonIgnore
    @Nullable
    private CustomerDto customer;

    @Indexed(unique = true)
    private String vin;

    @Indexed(unique = true)
    private String regNo;

    private String status;

    public VehicleDto() {
        this.status = "Connected";
    }


}
