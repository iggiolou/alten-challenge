package se.alten.challenge.dto;

import lombok.Data;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Data
public class CustomerDto {

    private String id;

    private String name;

    private String address;

    private List<VehicleDto> vehicles;

}
