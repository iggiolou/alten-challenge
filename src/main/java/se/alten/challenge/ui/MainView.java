package se.alten.challenge.ui;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import se.alten.challenge.dto.CustomerDto;
import se.alten.challenge.dto.VehicleDto;
import se.alten.challenge.service.api.CustomerService;
import se.alten.challenge.service.api.VehicleService;

import java.util.List;

/**
 * @author viktoriyadoroshenko
 * @since 2019-06-02
 */
@Route("main")
@UIScope
@SpringComponent
public class MainView extends VerticalLayout {


    private Button addNewVehicleButton = new Button("Add vehicle");
    private Button addNewCustomerButton = new Button("Add customer");
    private CustomerService customerService;
    private VehicleService vehicleService;
    private Grid<VehicleDto> vehicleGrid;

    MainView(CustomerService customerService, VehicleService vehicleService) {
        this.vehicleService = vehicleService;
        this.customerService = customerService;

        FormLayout mainLayout = createHeaderLayout();
        vehicleGrid = createVehicleGrid();
        add(mainLayout, vehicleGrid);

    }

    private FormLayout createHeaderLayout() {
        FormLayout mainLayout = new FormLayout();
        Text text = new Text("Monitoring display");
        mainLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("210em", 2)
        );
        mainLayout.addFormItem(text, "");
        mainLayout.addFormItem(addNewVehicleButton, "");
        mainLayout.addFormItem(addNewCustomerButton, "");

        addNewVehicleButton.addClickListener(event -> {
            Dialog addNewVehicleWindow = vehiclePopUp(null);
            addNewVehicleWindow.open();
        });

        addNewCustomerButton.addClickListener(event -> {
            Dialog addNewCustomerWindow = addNewCustomerButtonPopUp();
            addNewCustomerWindow.open();
        });

        return mainLayout;

    }

    private Dialog addNewCustomerButtonPopUp() {

        Dialog dialog = new Dialog(new Label("Add new customer"));
        FormLayout addCustomer = new FormLayout();

        TextField customerName = new TextField("Name");
        TextArea customerAddress = new TextArea("Address");
        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel", event -> dialog.close());

        saveButton.addClickListener(event -> {
            CustomerDto customerDto = new CustomerDto();
            customerDto.setName(customerName.getValue());
            customerDto.setAddress(customerAddress.getValue());
            customerService.save(customerDto);
            Notification.show("Customer created", 1000, Notification.Position.MIDDLE);
            dialog.close();

        });

        addCustomer.add(customerName, customerAddress, saveButton, cancelButton);
        dialog.add(addCustomer);

        return dialog;

    }

    private Grid<VehicleDto> createVehicleGrid() {

        vehicleGrid = new Grid<>();
        List<VehicleDto> vehicles = vehicleService.findAll();
        vehicleGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
        vehicleGrid.addColumn(VehicleDto::getVin).setHeader("VIN").setSortable(true);
        vehicleGrid.addColumn(VehicleDto::getRegNo).setHeader("Registration Number").setSortable(true);
        vehicleGrid.addColumn(VehicleDto::getStatus).setHeader("Status").setSortable(true);
        vehicleGrid.addColumn(x -> x.getCustomer() == null ? "N/A" : x.getCustomer().getName()).setHeader("Customer").setSortable(true);
        vehicleGrid.addColumn(new ComponentRenderer<>(item -> layout())).setHeader("Action");
        vehicleGrid.setItems(vehicles);
        return vehicleGrid;

    }

    private Dialog vehiclePopUp(VehicleDto current) {

        String create = "Add new vehicle";
        String update = "Edit for ";

        TextField vehicleVin = new TextField("VIN");
        TextField regNo = new TextField("Reg No");
        ComboBox<CustomerDto> customerDtoComboBox = new ComboBox<>("Customer");
        customerDtoComboBox.setItemLabelGenerator(CustomerDto::getName);
        customerDtoComboBox.setItems(customerService.findAll());
        FormLayout addVehicle = new FormLayout();
        Button saveButton = new Button("Save");

        Dialog dialog;

        if (current != null) {
            dialog = new Dialog(new Label(update + current.getVin()));

            vehicleVin.setValue(current.getVin());
            regNo.setValue(current.getRegNo());
            customerDtoComboBox.setValue(current.getCustomer());

        } else {
            dialog = new Dialog(new Label(create));
        }

        Button cancelButton = new Button("Cancel", event -> dialog.close());

        saveButton.addClickListener((ComponentEventListener<ClickEvent<Button>>) event -> {
            VehicleDto vehicleDto = new VehicleDto();
            String message;

            if (current != null) {
                vehicleDto.setId(current.getId());
                message = "Vehicle updated";
            } else {
                message = "Vehicle added";
            }
            vehicleDto.setVin(vehicleVin.getValue());
            vehicleDto.setRegNo(regNo.getValue());
            vehicleDto.setCustomer(customerDtoComboBox.getValue());
            if (current == null) {
                vehicleService.save(vehicleDto);
            } else {
                vehicleService.update(vehicleDto);
            }
            Notification.show(message, 1000, Notification.Position.MIDDLE);
            dialog.close();
            vehicleGrid.getEditor().refresh();
            vehicleGrid.setItems(vehicleService.findAll());

        });

        addVehicle.add(vehicleVin, regNo, customerDtoComboBox, saveButton, cancelButton);
        addVehicle.setResponsiveSteps(new FormLayout.ResponsiveStep("0", 1));
        dialog.add(addVehicle);
        return dialog;

    }

    private Button createDeleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.addClickListener(event -> vehicleGrid.getSelectionModel()
                .getFirstSelectedItem().ifPresent(x -> {
                    vehicleService.delete(x);
                    vehicleGrid.setItems(vehicleService.findAll());
                    Notification.show("Vehicle deleted", 1000, Notification.Position.MIDDLE);
                }));
        return deleteButton;
    }

    private Button createEditButton() {
        Button editButton = new Button("Edit");
        editButton.addClickListener(editClick -> vehicleGrid.getSelectionModel()
                .getFirstSelectedItem().ifPresent(x -> vehiclePopUp(x).open()));
        return editButton;
    }

    private HorizontalLayout layout() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.add(createEditButton());
        layout.add(createDeleteButton());
        return layout;
    }

}
