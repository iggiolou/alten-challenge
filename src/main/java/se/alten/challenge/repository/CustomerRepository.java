package se.alten.challenge.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import se.alten.challenge.entity.Customer;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */

public interface CustomerRepository extends MongoRepository<Customer, String> {

    void deleteById(String id);

}
