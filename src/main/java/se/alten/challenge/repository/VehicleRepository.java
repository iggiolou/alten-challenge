package se.alten.challenge.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import se.alten.challenge.entity.Vehicle;

import java.util.Optional;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-24
 */

public interface VehicleRepository extends MongoRepository<Vehicle, String> {

    void deleteById(String id);

    Optional<Vehicle> findByVin(String vin);

    Optional<Vehicle> findByRegNo(String regNo);
}
