package se.alten.challenge.configuration;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author viktoriyadoroshenko
 * @since 2019-05-14
 */
@Configuration
@EnableMongoRepositories(basePackages = "se.alten.challenge.repository")
public class MongoConfiguration extends AbstractMongoConfiguration {


    @Value("${spring.data.mongodb.database}")
    private String databaseName;

    @Value("${spring.data.mongodb.host}")
    private String host;

    @Value("${spring.data.mongodb.port}")
    private String port;

    @Value("${spring.data.mongodb.username}")
    private String userName;

    @Value("${spring.data.mongodb.password}")
    private String password;

    @Override
    public MongoClient mongoClient() {
        return new MongoClient(host, Integer.parseInt(port));
    }

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }


}
